//
//  ViewController.h
//  GitUseExample
//
//  Created by Sanjay  on 11/11/14.
//  Copyright (c) 2014 Sanjay . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)clickTheButton:(id)sender;

@end


//
//  ViewController.m
//  GitUseExample
//
//  Created by Sanjay  on 11/11/14.
//  Copyright (c) 2014 Sanjay . All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)clickTheButton:(id)sender
{
    NSLog(@"This is a Git tutorial commit 5");
}

@end
